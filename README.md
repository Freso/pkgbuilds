Moved (back) to GitHub
======================

Since I use GitHub far more than BitBucket and I generally, usually agree more
with GitHub than BitBucket/Atlassian, I have decided to move the repository
back to GitHub.

My PKGBUILDs repository can now be found here:
  https://github.com/Freso/PKGBUILDs

Feel free to fork and make pull requests over there.

Cheers!  
*-- [Freso](http://freso.dk/)*
